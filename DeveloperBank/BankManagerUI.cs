﻿namespace DeveloperBank
{
    public class BankManagerUI
    {
        public BankQueueManager bankManager;
        public BankManagerUI()
        {
            int maxEconomyQueue = 10;
            int maxVIPQueue = 10;
            bankManager = new BankQueueManager(maxEconomyQueue, maxVIPQueue);
        }

        public void AddCustomerToTheQueue()
        {
            Console.WriteLine("nhập dữ liệu từ người dùng");

            bankManager.AddCustomerToTheQueue(null);
        }

        /// <summary>
        /// 2.Gọi tên khách hàng kế tiếp (VIP)
        /// </summary>
        /// <returns></returns>
        public void GetNextVIPCustomer()
        {
            Console.WriteLine("Gọi tên khách hàng kế tiếp (VIP)");
            var vip = bankManager.GetNextVIPCustomer();

            // TODO: hiển thị thông tin khách hàng được gọi ra
        }

        /// <summary>
        /// 3.Gọi tên khách hàng kế tiếp (Thường).
        /// </summary>
        /// <returns></returns>
        public void GetNextEconomyCustomer()
        {
            Console.WriteLine("3.Gọi tên khách hàng kế tiếp (Thường).");
            var economy = bankManager.GetNextEconomyCustomer();

            // TODO: hiển thị thông tin khách hàng được gọi ra
        }

        /// <summary>
        /// 4.Khách hàng sắp tới.
        /// </summary>
        public void GetNextReadyCustomer()
        {
            Console.WriteLine("4.Khách hàng sắp tới.");
            var readyCustomer = bankManager.GetNextReadyCustomer();

            // TODO: hiển thị thông tin khách hàng chuẩn bị được gọi ra
        }

        /// <summary>
        /// 5.Thống kê
        /// </summary>
        public void ReportSystem()
        {
            Console.WriteLine("5.Thống kê");
            var report = bankManager.ReportSystem();
            // TODO: hiển thị thông tin report
        }
    }
}
